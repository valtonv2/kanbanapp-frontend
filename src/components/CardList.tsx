/* eslint-disable no-underscore-dangle */
/* eslint-disable no-restricted-globals */
import React, {
  ChangeEvent, FunctionComponent, useRef, useState,
} from 'react';
import styled from 'styled-components';
import {
  Droppable, Draggable,
} from 'react-beautiful-dnd';
import * as mongoose from 'mongoose';
import { useDispatch } from 'react-redux';
import Card from './Card';
import Editable from './Editable';
import DropdownMenu from './DropdownMenu';
import { BoardData } from '../types/kanban/boards';
import { CardListData } from '../types/kanban/cardlists';
import { CardData } from '../types/kanban/cards';
import { deleteCardList, updateCardList } from '../redux/cardlists/cardListActions';
import { addCard } from '../redux/cards/cardActions';

interface CardListProps {
  index: number,
  listData: CardListData,
  boardData: BoardData,
}

const CardListContainer = styled.div`
margin: 8.5rem .5rem 0rem;
flex-grow: 0;
flex-shrink: 0;
flex-basis: 10%;
min-width: 14rem;
background-color: #f7f7ff;
`;

const CardListDropArea = styled.div`
min-height: 1rem;
max-height: 75vh;
overflow: auto;
`;

const CardListDragArea = styled.div`
border-style: outset;
border-width: 1px;
border-radius: 3px;
`;

const CardListTopArea = styled.div`
margin: .5rem;
display: grid;
grid-template-columns: 85% 15%;
`;

const CardListHeader = styled.h3`
margin: 0 .5rem 0 0;
font-family: Arial, Helvetica, sans-serif;
grid-column: 1 / 1;
`;

const CardListNameInput = styled.input`
  display: block;
  margin: 0 .5rem 0 0;
`;

const CardListFunctionArea = styled.div`
grid-column: 2 / 2;
position: relative;
`;

const CardAddButton = styled.button`
margin: .5rem;
width: 92%;
height: 10%;
border: none;
border-radius: 3px;
background-color: #4CAF50;
`;

const OptionButton = styled.button`
border: none;
border-radius: .5rem;
background-color: rgba(173, 173, 173, 0.7);
`;

const DropDownButton = styled.button`
width: 100%;
height: 2.5rem;
border: none;
background-color: white;

&:hover {
  background-color: lightgray;
}
`;

const CardList: FunctionComponent<CardListProps> = (props) => {

  const { _id, cards } = props.listData;
  const [name, setName] = useState(props.listData.name);
  const dispatch = useDispatch();
  const nameref: React.MutableRefObject<any> = useRef<any>();
  const defaultCard: CardData = {
    _id: mongoose.Types.ObjectId().toString(),
    name: 'New task',
    description: '',
    users: [],
    tags: [],
  };

  const onCardAdd = () => {
    dispatch(addCard(_id, defaultCard));
  };

  const onCardListDelete = (list: CardListData) => () => {
    if (confirm('Are you sure you want to delete this cardlist?')) {
      dispatch(deleteCardList(props.boardData._id, list));
    }
  };

  const onCardListNameChange = (e: ChangeEvent<HTMLInputElement>) => setName(e.target.value);

  const onCardListNameBlur = () => dispatch(
    updateCardList(
      props.boardData._id,
      { ...props.listData, 
        name: name === '' ? props.listData.name : name
      }
    )
  );

  return (
    <Draggable key={_id} draggableId={_id} index={props.index}>
      {(provideddraggable) => (
        <CardListContainer
          ref={provideddraggable.innerRef}
          {...provideddraggable.draggableProps}
          {...provideddraggable.dragHandleProps}
        >
          <CardListDragArea>
            <CardListTopArea>
              <CardListHeader>
                <Editable placeholder="New list" text={name} childRef={nameref}>
                  <CardListNameInput
                    ref={nameref}
                    type="text"
                    name="Card list header"
                    placeholder="Card list header"
                    value={name}
                    onChange={onCardListNameChange}
                    onBlur={onCardListNameBlur}
                  />
                </Editable>
              </CardListHeader>
              <CardListFunctionArea>
                <DropdownMenu>
                  <OptionButton type="button" id="main">...</OptionButton>
                  <DropDownButton type="button" onClick={onCardListDelete(props.listData)}>
                    Delete
                  </DropDownButton>
                  <DropDownButton type="button">f2</DropDownButton>
                  <DropDownButton type="button">f3</DropDownButton>
                </DropdownMenu>
              </CardListFunctionArea>
            </CardListTopArea>
            <Droppable droppableId={_id} type="CARD">
              {(provided) => (
                <CardListDropArea
                  ref={provided.innerRef}
                  {...provided.droppableProps}
                >
                  {cards.map(card => (
                    <Card
                      cardData={card}
                      index={cards.indexOf(card)}
                      key={card._id}
                    />
                  ))}
                  {provided.placeholder}
                </CardListDropArea>
              )}
            </Droppable>
            <CardAddButton
              type="button"
              onClick={onCardAdd}
            >
              Add card
            </CardAddButton>
          </CardListDragArea>
        </CardListContainer>
      )}
    </Draggable>
  );
};

export default CardList;
