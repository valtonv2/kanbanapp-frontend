/* eslint-disable no-underscore-dangle */
import React, {
  ChangeEvent, FunctionComponent, useRef, useState,
} from 'react';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { CardData } from '../types/kanban/cards';
import Editable from './Editable';
import { CardListData } from '../types/kanban/cardlists';
import ExpandableTextArea from './ExpandableTextArea';
import { deleteCard, updateCard } from '../redux/cards/cardActions';
import { changeCurrentCard } from '../redux/views/viewActions';
import { RootReducerState } from '../types/redux/reducers';
import userService from '../services/comms/userService';

const ModalContainer = styled.div`
display: block;
position: fixed;
width: 100%;
height: 100%;
overflow: auto;
background-color: rgba(128,128,128,0.8);
z-index: 2;
`;

const ModalContentContainer = styled.div`
display: block;
position: absolute;
z-index: 1;
top: 10%;
left: 25%;
right: 25%;
min-height: 20rem;
background-color: #f7f7ff;
`;

const VerticalFlexContainer = styled.div`
width: 100%;
height: 100%;
display: flex;
flex-direction: column;
`;

const CardImage = styled.img`
width: 100%;
object-fit: cover;
`;

const HorizontalFlexContainer = styled.div`
width: 100%;
height: 100%;
min-height: 20rem;
display: flex;
flex-direction: row;
`;

const CardTextContent = styled.div`
width: 70%;
height: 100%;
min-height: 20rem;
display: flex;
flex-direction: column;
`;

const HeaderRow = styled.div`
background-color: #f7f7ff;
`;

const DescriptionArea = styled.div`
background-color: #f7f7ff;
`;

const SideArea = styled.div`
width: 30%;
min-height: 20rem;
display: flex;
flex-grow: 1;
align-items: center;
flex-direction: column;
background-color: #495867;
`;

const CardInfoHeaderText = styled.h1`
font-family: Arial, Helvetica, sans-serif;
margin: 1rem;
`;

const CardHeaderInput = styled.input`
width: 100%;
font-size: 1.5rem;
font-weight: bold;
`;

const DescriptionText = styled.div`
font-family: Arial, Helvetica, sans-serif;
margin: 1rem;
`;

const CardFunctionButton = styled.button`
margin: .25rem 0 .25rem 0;
width: calc(100% - 1rem);
min-height: 3rem;
flex-grow: 0;
flex-shrink: 0;
background-color: #F7F7FF;
border-style: none;
border-radius: .5rem;
`;

interface CardInfoViewProps {
  cardList: CardListData
  card: CardData
}

const CardInfoView: FunctionComponent<CardInfoViewProps> = (props) => {

  const { card, cardList } = props;
  const [name, setCardName] = useState(card.name);
  const [description, setCardDescription] = useState(card.description);
  const nameref:React.MutableRefObject<any> = useRef<any>();
  const descref: React.MutableRefObject<any> = useRef<any>();
  const fileref: React.MutableRefObject<any> = useRef<any>();
  const imageUrl = card.additionalData?.find(o => Object.keys(o).includes('image'))?.image;

  const dispatch = useDispatch();
  const user = useSelector((state: RootReducerState) => { return (state.userstate.user); });

  const onCardNameChange = (e: ChangeEvent<HTMLInputElement>) => setCardName(e.target.value);

  const onCardDescriptionChange = (e: ChangeEvent<HTMLTextAreaElement>) => setCardDescription(e.target.value);

  const onPropertyBlur = (e: React.FocusEvent) => {
    e.preventDefault();

    const updatedCard = {
      ...card, 
      name: name === '' ? card.name : name, 
      description 
    };
    dispatch(updateCard(cardList._id, updatedCard));
    dispatch(changeCurrentCard(updatedCard));
  };

  const onModalClick = (e: React.MouseEvent) => {
    e.stopPropagation();
  };

  const onCardDelete = () => {
    // eslint-disable-next-line no-alert
    // eslint-disable-next-line no-restricted-globals
    if (confirm('Are you sure you want to delete this card?')) {
      dispatch(changeCurrentCard(null));
      dispatch(deleteCard(cardList._id, card));
    }
  };

  const onAddCoverClick = (e: React.MouseEvent) => {
    e.preventDefault();
    e.stopPropagation();
    fileref.current.click();
  };

  const onClearCoverClick = async (e: React.MouseEvent) => {
    e.preventDefault();
    e.stopPropagation();

    if (!imageUrl) return;

    const updatedCard = { ...card, additionalData: [] };
    dispatch(updateCard(cardList._id, updatedCard));
    dispatch(changeCurrentCard(updatedCard));

    if (user.usertoken) await userService.deleteImageOnline(user.usertoken, imageUrl);
  };

  const onClose = (e: React.MouseEvent) => {
    e.preventDefault();
    e.stopPropagation();
    dispatch(changeCurrentCard(null));
  };

  const onFileChange = async (e: React.ChangeEvent<HTMLInputElement>) => {
    e.preventDefault();
    e.stopPropagation();

    const file = e.target.files?.item(0);
    if (file && user.usertoken) {

      const formData = new FormData();
      formData.append('file', file);
      await userService.addImageOnline(user, user.usertoken, formData);

      const path = `${process.env.REACT_APP_SERVERURL}/users/${user._id}/images/${file.name}`;
      const oldPath = imageUrl;

      const updatedCard = { ...card, additionalData: [{ image: path }] };
      dispatch(updateCard(cardList._id, updatedCard));
      dispatch(changeCurrentCard(updatedCard));

      if (oldPath && oldPath !== path) await userService.deleteImageOnline(user.usertoken, oldPath);
    }
  };

  return (
    <ModalContainer onClick={onClose}>
      <ModalContentContainer onClick={onModalClick}>
        <VerticalFlexContainer>
          {imageUrl && <CardImage src={imageUrl} alt={imageUrl} />}
          <HorizontalFlexContainer>
            <CardTextContent>
              <HeaderRow>
                <CardInfoHeaderText>
                  <Editable placeholder="New task" text={name} childRef={nameref}>
                    <CardHeaderInput
                      ref={nameref}
                      type="text"
                      name="Card header"
                      placeholder="New task"
                      value={name}
                      onChange={onCardNameChange}
                      onBlur={onPropertyBlur}
                    />
                  </Editable>
                </CardInfoHeaderText>
              </HeaderRow>
              <DescriptionArea>
                <DescriptionText>
                  <Editable placeholder="Card description" text={description} childRef={descref}>
                    <ExpandableTextArea
                      childRef={descref}
                      name="Card description"
                      placeholder="Card description"
                      value={description}
                      onChange={onCardDescriptionChange}
                      onBlur={onPropertyBlur}
                    />
                  </Editable>
                </DescriptionText>
              </DescriptionArea>
            </CardTextContent>
            <SideArea>
              <DescriptionText>Functions</DescriptionText>
              <CardFunctionButton type="button" onClick={onCardDelete}>Delete card</CardFunctionButton>
              <CardFunctionButton type="button" onClick={onAddCoverClick}>Add cover image</CardFunctionButton>
              {imageUrl && <CardFunctionButton type="button" onClick={onClearCoverClick}>Clear cover image</CardFunctionButton>}
              <input
                type="file"
                style={{ display: 'none' }}
                ref={fileref}
                onChange={onFileChange}
                accept="image/png, image/jpeg"
              />
            </SideArea>
          </HorizontalFlexContainer>
        </VerticalFlexContainer>
      </ModalContentContainer>
    </ModalContainer>
  );
};

export default CardInfoView;
