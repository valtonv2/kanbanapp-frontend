/* eslint-disable no-param-reassign */
import React, { FunctionComponent, useEffect } from 'react';
import styled from 'styled-components';

const TextArea = styled.textarea`
    width: 100%;
`;

const ExpandableTextArea: FunctionComponent<any> = (props) => {
  const updateHeight = (textField: HTMLTextAreaElement) => {
    // Reset height
    textField.style.height = 'inherit';

    // Get computed style
    const computedStyle = window.getComputedStyle(textField);

    // Find out needed height
    const height = parseInt(computedStyle.borderTopWidth, 10)
                    + parseInt(computedStyle.paddingTop, 10)
                    + parseInt(computedStyle.borderBottomWidth, 10)
                    + parseInt(computedStyle.paddingBottom, 10)
                    + textField.scrollHeight;

    textField.style.height = `${height}px`;
  };

  const onInput = (event: React.FormEvent<HTMLTextAreaElement>) => {
    if (event.target instanceof HTMLTextAreaElement) {
      updateHeight(event.target);
    }
  };

  useEffect(() => {
    updateHeight(props.childRef.current)
  });

  // TODO: Make use of childRef more secure
  return (
    <TextArea ref={props.childRef} onInput={(event) => onInput(event)} onClick={(event) => onInput(event)} {...props} />
  );
};

export default ExpandableTextArea;
