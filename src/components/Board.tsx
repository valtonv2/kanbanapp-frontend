/* eslint-disable no-underscore-dangle */
/* eslint-disable no-restricted-globals */
/* eslint-disable no-alert */
import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import { useDispatch } from 'react-redux';
import * as mongoose from 'mongoose';
import CardList from './CardList';

import handleDrops from '../services/kanbanlogic/dndUtils';
import { BoardData } from '../types/kanban/boards';
import { CardListData } from '../types/kanban/cardlists';
import { updateBoard } from '../redux/boards/boardActions';
import { addCardList } from '../redux/cardlists/cardListActions';

const BoardDropArea = styled.div`
`;

const BoardContainer = styled.div`
display: flex;
flex-direction: row;
align-items: flex-start;
min-height: 100vh;
min-width: 100vw;
width: 100%;
overflow: auto;
background-size: cover;
`;

const ListAddButton = styled.button`
margin: 8.5rem 1rem 1rem;
min-width: 12rem;
min-height: 4rem;
border-style: none;

`;

interface BoardProps {
  boardData: BoardData,
}

const Board: FunctionComponent<BoardProps> = (props) => {

  const { _id, lists, additionalData } = props.boardData;
  const dispatch = useDispatch();

  const onCardListAdd = () => {
    // When a card list is added we add a default card that can then be customized.
    const defaultCardList: CardListData = {
      _id: mongoose.Types.ObjectId().toString(),
      name: 'New list',
      cards: [],
    };
    dispatch(addCardList(_id, defaultCardList));
  };

  const imageUrl = additionalData?.find(o => Object.keys(o).includes('image'))?.image;
  const backgroundStyle: React.CSSProperties = imageUrl ? { backgroundImage: `url(${imageUrl})` } : {};

  return (
    <DragDropContext onDragEnd={(result) => dispatch(updateBoard(handleDrops(result, props.boardData)))}>
      <Droppable droppableId={_id} type="CARDLIST" direction="horizontal">
        {(provided) => (
          <BoardDropArea
            ref={provided.innerRef}
            {...provided.droppableProps}
          >
            <BoardContainer style={backgroundStyle}>
              {lists.map(list => (
                <CardList
                  key={list._id}
                  index={lists.indexOf(list)}
                  listData={list}
                  boardData={props.boardData}
                />
              ))}
              {provided.placeholder}
              <ListAddButton onClick={onCardListAdd}>Add list</ListAddButton>
            </BoardContainer>
          </BoardDropArea>
        )}
      </Droppable>
    </DragDropContext>
  );
};

export default Board;
