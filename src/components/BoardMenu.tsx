/* eslint-disable no-underscore-dangle */
import React, { FunctionComponent } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import styled from 'styled-components';
import * as mongoose from 'mongoose';
import { addBoard } from '../redux/boards/boardActions';
import { changeCurrentBoard, changeView } from '../redux/views/viewActions';
import AppView from '../types/appview';
import { BoardData } from '../types/kanban/boards';
import { RootReducerState } from '../types/redux/reducers';

const BoardMenuContainer = styled.div`
padding: 3.5rem .5rem .5rem;
`;

const BoardMenuHeaderText = styled.h1`
font-family: Arial, Helvetica, sans-serif;
margin: .5rem;
`;

const BoardButton = styled.div`
margin: 0 0 .5rem 0;
min-height: 9rem;
border-style: outset;
border-width: 1px;
border-radius: 3px;
background-color: #f7f7ff;
background-size: cover;
`;

const AddBoardButton = styled.button`
width: 100%;
min-height: 9rem;
border-style: outset;
border-width: 1px;
border-radius: 3px;
background-color: lightgray;
`;

const BoardButtonHeader = styled.h2`
margin: 1rem;
`;

interface BoardMenuProp {
  boards: BoardData[],
}

const BoardMenu: FunctionComponent<BoardMenuProp> = (props) => {

  const { boards } = props;
  const user = useSelector((state: RootReducerState) => state.userstate.user);
  const dispatch = useDispatch();

  const onBoardSelection = (board: BoardData) => () => {
    dispatch(changeCurrentBoard(board));
    dispatch(changeView(AppView.BOARDVIEW));
  };

  const onBoardAdd = () => {
    const defaultBoard: BoardData = {
      _id: mongoose.Types.ObjectId().toString(),
      name: `New board ${boards.length + 1}`,
      description: 'Board description',
      users: [user._id],
      lists: [],
    };
    dispatch(addBoard(defaultBoard));
  };

  const getBackgroundStyle = (b: BoardData) => {
    const imageUrl = b.additionalData?.find(o => Object.keys(o).includes('image'))?.image;
    const backgroundStyle: React.CSSProperties = imageUrl ? { backgroundImage: `linear-gradient(rgba(255, 255, 255, 0.4), rgba(255, 255, 255, 0.0)), url(${imageUrl})` } : {};
    return backgroundStyle;
  };

  return (
    <BoardMenuContainer>
      <BoardMenuHeaderText>Board selection</BoardMenuHeaderText>
      {boards && boards.map(b => (
        <BoardButton onClick={onBoardSelection(b)} key={b._id} style={getBackgroundStyle(b)}>
          <BoardButtonHeader>{b.name}</BoardButtonHeader>
        </BoardButton>
      ))}
      <AddBoardButton onClick={onBoardAdd}>Add board</AddBoardButton>
    </BoardMenuContainer>
  );
};

export default BoardMenu;
