import React, { FunctionComponent } from 'react';
import styled from 'styled-components';

const ModalContainer = styled.div`
display: block;
position: fixed;
width: 100%;
height: 100%;
background-color: rgba(128,128,128,0.8);
z-index: 1;
`;

const ModalContentContainer = styled.div`
display: block;
position: fixed;
z-index: 1;
top: 10%;
left: 25%;
right: 25%;
bottom: 10%;
background-color: #f7f7ff;
`;

interface FileUploadModalProps {
    onFileUploaded: () => void;
    onFileChange: () => void;
    onModalClick: () => void;
}

const FileUploadModal: FunctionComponent<FileUploadModalProps> = ({ onFileUploaded, onFileChange, onModalClick }) => {

  return (
    <ModalContainer onClick={onModalClick}>
      <ModalContentContainer>
        <h1>Upload file</h1>
        <input type="file" onChange={onFileChange} />
        <button type="submit" onClick={onFileUploaded}>
          Upload
        </button>
      </ModalContentContainer>
    </ModalContainer>
  );
};

export default FileUploadModal;
