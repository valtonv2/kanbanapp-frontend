/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
import React, { FunctionComponent, useState } from 'react';
import styled from 'styled-components';

interface DropdownProp {
}

const DropdownContainer = styled.div`
`;

const MenuModal = styled.div`
    display: block;
    position: absolute;
    z-index: 1;
    width: 12rem;
    background-color: white;
    border: outset;
    border-radius: .2rem;
    border-width: .1rem;
`;

const DropdownMenu: FunctionComponent<DropdownProp> = (props) => {
  const [menuVisible, setVisibility] = useState(false);

  const buttons = React.Children.toArray(props.children)
    .filter(c => !(c instanceof HTMLButtonElement))
    .map(c => c as HTMLButtonElement);

  if (buttons.length === 0) throw new Error('DropDownMenu did not have button component children!');

  const mainButton = (buttons[0]);

  buttons.shift();

  return (
    <DropdownContainer
      onBlur={(event: React.FocusEvent<HTMLElement>) => {
        if (!event.currentTarget.contains(event.relatedTarget as Node)) setVisibility(false);
      }}
      onClick={(event) => {
        if (event.currentTarget.contains(event.target as Node) && event.target !== event.currentTarget) setVisibility(!menuVisible);
      }}
    >
      {mainButton}
      <MenuModal onClick={() => setVisibility(false)} style={menuVisible ? { display: 'block' } : { display: 'none' }}>
        {buttons}
      </MenuModal>
    </DropdownContainer>

  );
};

export default DropdownMenu;
