import React, { FunctionComponent } from 'react';
import styled from 'styled-components';
import { Draggable } from 'react-beautiful-dnd';
import { useDispatch } from 'react-redux';
import { CardData } from '../types/kanban/cards';
import { changeCurrentCard } from '../redux/views/viewActions';

const CardContainer = styled.div`
margin: 8px;
min-width: 180px;
min-height: 60px;
border-style: outset;
border-width: 1px;
border-radius: 3px;
background-color: white;
`;

const CardImage = styled.img`
max-width: 100%;
min-width: 100%;
`;

const CardHeader = styled.p`
  font-family: Arial, Helvetica, sans-serif;
  margin: 4px;
`;

interface CardDataProp {
  index: number,
  cardData: CardData,
}

const Card: FunctionComponent<CardDataProp> = (props) => {

  const { _id, name, additionalData } = props.cardData;
  const dispatch = useDispatch();

  const onCardClick = (e: React.MouseEvent) => {
    e.stopPropagation();
    dispatch(changeCurrentCard(props.cardData));
  };

  const imageUrl = additionalData?.find(o => Object.keys(o).includes('image'))?.image;

  return (
    <Draggable key={_id} draggableId={_id} index={props.index}>
      {(provided) => (
        <CardContainer
          ref={provided.innerRef}
          {...provided.draggableProps}
          {...provided.dragHandleProps}
          onClick={onCardClick}
        >
          {imageUrl && <CardImage src={imageUrl} alt={imageUrl} />}
          <CardHeader>
            {name}
          </CardHeader>
        </CardContainer>
      )}
    </Draggable>
  );
};

export default Card;
