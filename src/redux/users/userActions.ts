import { ThunkAction } from 'redux-thunk';
import * as jwt from 'jsonwebtoken';
import axios from 'axios';
import loginService from '../../services/comms/loginService';
import userService from '../../services/comms/userService';
import { isUser } from '../../services/validation/kanban';
import { isTokenData, isTokenContents } from '../../services/validation/tokens';
import { User } from '../../types/kanban/users';
import {
  UserActionTypes, SIGNUP_USER, LOGIN_USER, LOGOUT_USER, UPDATE_USER, CHANGE_USER_STATUS, ChangeErrorAction, CLEAR_BOARDS, ClearBoardsAction,
} from '../../types/redux/actions';
import { RootReducerState } from '../../types/redux/reducers';
import RequestState from '../../types/requeststate';
import changeError from '../errors/errorActions';
import isError from '../../services/validation/error';






export const signupUser = (
  username: string,
  password: string,
): ThunkAction<void, RootReducerState, unknown, UserActionTypes | ChangeErrorAction> => {
  return async (dispatch) => {

    try {

      const newUser = await userService.createUserOnline({ username, password });
      if (!newUser) throw new Error('No server response.');
      if (!isUser(newUser)) throw new Error('Malformed server response.');

      dispatch({ type: SIGNUP_USER });

    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }
    }

  };
};

export const loginUser = (
  username: string,
  password: string,
): ThunkAction<void, RootReducerState, unknown, UserActionTypes | ChangeErrorAction> => {
  return async (dispatch) => {

    try {

      const token = await loginService.login({ username, password });
      if (!token) throw new Error('No server response.');
      if (!isTokenData(token)) throw new Error('Malformed token response.');

      if (!process.env.REACT_APP_SECRET) throw new Error('No SECRET. Check .env variables.');
      const decoded = jwt.verify(token.usertoken, process.env.REACT_APP_SECRET);
      if (!isTokenContents(decoded)) throw new Error('Malformed token contents');

      localStorage.setItem('userToken', token.usertoken);

      const userData = await userService.getuserOnline(decoded.id, token.usertoken);
      if (!isUser(userData)) throw new Error('Malformed server response');

      dispatch({ type: LOGIN_USER, payload: { ...userData, usertoken: token.usertoken } });

    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }
    }
  };
};

export const loginExistingUser = (): ThunkAction<void, RootReducerState, unknown, UserActionTypes | ChangeErrorAction> => {
  return async (dispatch) => {

    try {
      console.log('Existing user login action');
      const token = localStorage.getItem('userToken');
      if (!token) return;
      console.log('found token');
      if (!process.env.REACT_APP_SECRET) throw new Error('No SECRET. Check .env variables.');
      const decoded = jwt.verify(token, process.env.REACT_APP_SECRET);
      if (!isTokenContents(decoded)) throw new Error('Malformed token contents');
      console.log('verified token');
      const userData = await userService.getuserOnline(decoded.id, token);
      if (!isUser(userData)) throw new Error('Malformed server response');
      console.log('got user');
      dispatch({ type: LOGIN_USER, payload: { ...userData, usertoken: token } });

    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }
    }
  };
};

export const logoutUser = (): ThunkAction<void, RootReducerState, unknown, UserActionTypes | ChangeErrorAction | ClearBoardsAction> => {
  return (dispatch) => {
    localStorage.clear();
    dispatch({ type: CLEAR_BOARDS });
    dispatch({ type: LOGOUT_USER });
  };
};

export const updateUser = (
  user: User,
): ThunkAction<void, RootReducerState, unknown, UserActionTypes | ChangeErrorAction> => {
  return async (dispatch, getState) => {

    try {

      const token = getState().userstate.user.usertoken;
      if (!token) throw new Error('No token for operation. Log in.');

      const updatedUser = await userService.updateUserOnline(user, token);
      if (!isUser(updatedUser)) throw new Error('Malformed server response.');

      dispatch({ type: UPDATE_USER, payload: { user: updatedUser } });

    } catch (error) {
      if (axios.isAxiosError(error)) {
        console.error(error);
        dispatch(changeError(error.response?.data.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      } else if (isError(error)) {
        console.error(error);
        dispatch(changeError(error.message));
        setTimeout(() => dispatch(changeError(null)), 2000);
      }
    }

  };
};

export const changeUserRequestStatus = (newStatus: RequestState): UserActionTypes => {
  return { type: CHANGE_USER_STATUS, payload: { status: newStatus } };
};

