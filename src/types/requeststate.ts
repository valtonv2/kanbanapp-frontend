enum RequestState {
    IDLE,
    LOADING,
    SUCCESS,
    FAILURE
}

export default RequestState;
