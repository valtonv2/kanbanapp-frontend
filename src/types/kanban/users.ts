/* eslint-disable no-underscore-dangle */

import { AdditionalData } from './additionaldata';

export type User = {
    _id: string, // Uuid or mongo generated
    username: string, // unique
    passwordhash: string, // Hashed form
    boards: string[] // Board ids
    settings: AdditionalData[] // Not in use quickly but at some point
}

export type NewUser = {
    username: string,
    password: string
}

export type TokenData = {
  usertoken: string
}

export type TokenContents = {
  username: string,
  id: string
}

export type LoginUser = User & (TokenData | {usertoken: undefined});
