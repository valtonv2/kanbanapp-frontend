import { AdditionalData } from './additionaldata';


export type CardData = {
    _id: string, // Uuid or mongo generated
    name: string,
    description: string,
    users: string[], // User ids
    tags: string[],
    additionalData?: AdditionalData[] // Time, color, other plugin data etc.
}

export type CardDTO = CardData

export type NewCardData = Omit<CardDTO, '_id'>
