export type AdditionalData = {
    [key: string]: string
};
