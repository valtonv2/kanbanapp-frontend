enum AppView {
    BOARDMENU = 'BOARDMENU',
    BOARDVIEW = 'BOARDVIEW',
    WELCOMEVIEW = 'WELCOMEVIEW'
}

export default AppView;
