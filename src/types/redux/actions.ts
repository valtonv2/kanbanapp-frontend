import AppView from '../appview';
import { BoardData } from '../kanban/boards';
import { CardListData } from '../kanban/cardlists';
import { CardData } from '../kanban/cards';
import { User, NewUser, LoginUser } from '../kanban/users';
import RequestState from '../requeststate';



// General types
export type KanbanActionTypes =
AddCardAction|
DeleteCardAction|
AddCardListAction|
DeleteCardListAction|
UpdateCardAction|
UpdateCardListAction|
AddBoardAction|
DeleteBoardAction|
UpdateBoardAction|
ChangeBoardStatusAction|
InitBoardsAction|
ClearBoardsAction

export type UserActionTypes =
  SignupUserAction|
  LoginUserAction|
  LogoutUserAction|
  UpdateUserAction|
  ChangeUserStatusAction

export type ViewActionTypes =
  ChangeViewAction|
  ChangeCurrentBoardAction|
  ChangeCurrentCardAction

// Board related types
export const ADD_BOARD = 'ADD_BOARD';
export const DELETE_BOARD = 'DELETE_BOARD';
export const UPDATE_BOARD = 'UPDATE_BOARD';
export const INIT_BOARDS = 'INIT_BOARDS';
export const CLEAR_BOARDS = 'CLEAR_BOARDS';

export interface BoardPayload {
  board: BoardData
}

export interface BoardArrayPayload {
  boards: BoardData[]
}

export interface AddBoardAction {
  type: typeof ADD_BOARD
  payload: BoardPayload
}

export interface DeleteBoardAction {
  type: typeof DELETE_BOARD
  payload: BoardPayload
}

export interface UpdateBoardAction {
  type: typeof UPDATE_BOARD
  payload: BoardPayload
}

export interface InitBoardsAction {
  type: typeof INIT_BOARDS
  payload: BoardArrayPayload
}

export interface ClearBoardsAction {
  type: typeof CLEAR_BOARDS
}



// Card list related types
export const ADD_CARDLIST = 'ADD_CARDLIST';
export const DELETE_CARDLIST = 'DELETE_CARDLIST';
export const UPDATE_CARDLIST = 'UPDATE_CARDLIST';

export interface CardListPayload {
  cardList: CardListData
  boardId: string
}

export interface AddCardListAction {
  type: typeof ADD_CARDLIST
  payload: CardListPayload
}

export interface DeleteCardListAction {
  type: typeof DELETE_CARDLIST
  payload: CardListPayload
}

export interface UpdateCardListAction {
  type: typeof UPDATE_CARDLIST
  payload: CardListPayload
}



// Card related types
export const ADD_CARD = 'ADD_CARD';
export const DELETE_CARD = 'DELETE_CARD';
export const UPDATE_CARD = 'UPDATE_CARD';

export interface CardPayload {
  card: CardData
  listId: string
}

export interface AddCardAction {
  type: typeof ADD_CARD;
  payload: CardPayload
}

export interface DeleteCardAction {
  type: typeof DELETE_CARD
  payload: CardPayload
}

export interface UpdateCardAction {
  type: typeof UPDATE_CARD
  payload: CardPayload
}



// User related types
export const LOGIN_USER = 'LOGIN_USER';
export const LOGOUT_USER = 'LOGOUT_USER';
export const SIGNUP_USER = 'SIGNUP_USER';
export const UPDATE_USER = 'UPDATE_USER';

export interface UserPayload {
  user: User
}

export interface NewUserPayload {
  user: NewUser
}

export interface SignupUserAction {
  type: typeof SIGNUP_USER
}

export interface LoginUserAction {
  type: typeof LOGIN_USER
  payload: LoginUser
}

export interface LogoutUserAction {
  type: typeof LOGOUT_USER
}

export interface UpdateUserAction {
  type: typeof UPDATE_USER
  payload: UserPayload
}



// Request status action types
export const CHANGE_USER_STATUS = 'CHANGE_USER_STATUS';
export const CHANGE_BOARD_STATUS = 'CHANGE_BOARD_STATUS';

export interface RequestStatusPayload {
  status: RequestState
}

export interface ChangeUserStatusAction {
  type: typeof CHANGE_USER_STATUS
  payload: RequestStatusPayload
}

export interface ChangeBoardStatusAction {
  type: typeof CHANGE_BOARD_STATUS
  payload: RequestStatusPayload
}



// Error related action types
export const CHANGE_ERROR = 'CHANGE_BOARD_STATUS';

export interface ErrorPayload {
  error: string|null
}

export interface ChangeErrorAction {
  type: typeof CHANGE_ERROR
  payload: ErrorPayload
}



// View related action types
export const CHANGE_VIEW = 'CHANGE_VIEW';
export const CHANGE_CURRENT_BOARD = 'CHANGE_CURRENT_BOARD';
export const CHANGE_CURRENT_CARD = 'CHANGE_CURRENT_CARD';


export interface ViewPayload {
  view: AppView
}

export interface PossibleBoardPayload {
  board: BoardData|null
}

export interface PossibleCardPayload {
  card: CardData|null
}

export interface ChangeViewAction {
  type: typeof CHANGE_VIEW
  payload: ViewPayload
}

export interface ChangeCurrentBoardAction {
  type: typeof CHANGE_CURRENT_BOARD
  payload: PossibleBoardPayload
}

export interface ChangeCurrentCardAction {
  type: typeof CHANGE_CURRENT_CARD
  payload: PossibleCardPayload
}



