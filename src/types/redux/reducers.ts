import AppView from '../appview';
import { BoardData } from '../kanban/boards';
import { CardData } from '../kanban/cards';
import { LoginUser } from '../kanban/users';
import RequestState from '../requeststate';

export type BoardReducerState = {
    boards: BoardData[]
    status: RequestState
}

export type UserReducerState = {
    user: LoginUser
    status: RequestState
}

export type ErrorReducerState = {
    error: string|null
}

export type ViewReducerState = {
    view: AppView,
    currentBoard: BoardData|null,
    currentCard: CardData|null
}

export type RootReducerState = {
    userstate: UserReducerState,
    boardstate: BoardReducerState,
    errorstate: ErrorReducerState,
    viewstate: ViewReducerState
}

