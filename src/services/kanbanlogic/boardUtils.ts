/* eslint-disable no-underscore-dangle */
import cloneDeep from 'lodash.clonedeep';
import { BoardData } from '../../types/kanban/boards';


// Board methods
const addBoard = (board: BoardData, boardList: BoardData[]) => {
  const cloneData = cloneDeep(boardList);
  cloneData.push(board);
  return cloneData;
};

const removeBoard = (board: BoardData, boardList: BoardData[]) => {
  const cloneData = cloneDeep(boardList);
  const boardIndex = cloneData.findIndex(b => b._id === board._id);
  if (boardIndex === -1) throw new Error('Removable board did not exist in board list.');
  cloneData.splice(boardIndex, 1);
  return cloneData;
};

const updateBoard = (board: BoardData, boardList: BoardData[]) => {
  const cloneData = cloneDeep(boardList);
  const boardIndex = cloneData.findIndex(b => b._id === board._id);
  if (boardIndex === -1) throw new Error('Updatable board did not exist in board list.');
  cloneData.splice(boardIndex, 1, board);
  return cloneData;
};

export default { addBoard, removeBoard, updateBoard };
