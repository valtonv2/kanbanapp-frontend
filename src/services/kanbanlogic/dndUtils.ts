/* eslint-disable no-underscore-dangle */
import cloneDeep from 'lodash.clonedeep';
import { DropResult } from 'react-beautiful-dnd';
import { BoardData } from '../../types/kanban/boards';
import { CardListData } from '../../types/kanban/cardlists';
import { CardData } from '../../types/kanban/cards';

// Helper method for moving card inside or between lists
const moveCard = (sl: CardData[], dl: CardData[], si: number, di: number) => {
  const movingCard = sl[si];
  sl.splice(si, 1); // Take card out of source list
  dl.splice(di, 0, movingCard); // Add card to destination
};

// Helper method for moving card list inside board
const moveCardList = (l: CardListData[], si: number, di: number) => {
  const movingCard = l[si];
  l.splice(si, 1); // Take card out of source list
  l.splice(di, 0, movingCard); // Add card to destination
};

// Handle card drop inside card list
const handleCardDrop = (result: DropResult, boardData: BoardData) => {
  if (!result.destination) return;
  const sourceListId = result.source.droppableId;
  const destinationListId = result.destination.droppableId;
  const sourceIndex = result.source.index;
  const destinationIndex = result.destination.index;
  const sourceList = boardData.lists.find(list => list._id === sourceListId);
  const destinationList = boardData.lists.find(list => list._id === destinationListId);
  if (!sourceList || !destinationList) throw Error('Broken Drag and Drop event');
  moveCard(
    sourceList.cards,
    destinationList.cards,
    sourceIndex,
    destinationIndex,
  );
};

// Handle card list drop inside board
const handleListDrop = (result: DropResult, boardData: BoardData) => {
  if (!result.destination) return;
  const sourceIndex = result.source.index;
  const destinationIndex = result.destination.index;
  moveCardList(boardData.lists, sourceIndex, destinationIndex);
};

// Function to handle both card and card list drops
const handleDrops = (
  result: DropResult,
  boardData: BoardData,
) => {
  const boardClone = cloneDeep(boardData);
  if (result.type === 'CARD') handleCardDrop(result, boardClone);
  if (result.type === 'CARDLIST') handleListDrop(result, boardClone);
  return boardClone;
};

export default handleDrops;
