/* eslint-disable no-underscore-dangle */
import axios from 'axios';
import { NewUser, User } from '../../types/kanban/users';

const baseUrl = `${process.env.REACT_APP_SERVERURL as string}/users`;

// User fetch
const getuserOnline = (id: string, userToken: string) => {
  return (axios.get(`${baseUrl}/${id}`, { headers: { authorization: userToken } }))
    .then(response => response.data);
};

// User creation
const createUserOnline = (newUser: NewUser) => {
  return (axios.post(baseUrl, newUser).then(response => response.data));
};

// User update
const updateUserOnline = (user: User, userToken: string) => {
  return (axios.put(`${baseUrl}/${user._id}`, user, { headers: { authorization: userToken } })
    .then(response => response.data));
};

// User deletion
const deleteUserOnline = (user: User, userToken: string) => {
  return (axios.delete(`${baseUrl}/${user._id}`, { headers: { authorization: userToken } })
    .then(response => response.data));
};

// Adding image
const addImageOnline = (user: User, userToken: string, formData: FormData) => {
  return (axios.post(`${baseUrl}/${user._id}/images`, formData, { headers: { authorization: userToken, contentType: 'multipart/form-data' } })
    .then(response => response));
};

// Deleting image
const deleteImageOnline = (userToken: string, imageAddress: string) => {
  return (axios.delete(imageAddress, { headers: { authorization: userToken } })
    .then(response => response));
};

export default {
  getuserOnline,
  createUserOnline,
  updateUserOnline,
  deleteUserOnline,
  addImageOnline,
  deleteImageOnline,
};
