import { TokenData, TokenContents } from '../../types/kanban/users';

export const isTokenData = (o: any): o is TokenData => {
  return (o.usertoken && typeof o.usertoken === 'string');
};

export const isTokenContents = (o: any): o is TokenContents => {
  return (o.username && o.id && typeof o.username === 'string' && typeof o.id === 'string');
};
