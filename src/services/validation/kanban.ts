/* eslint-disable no-underscore-dangle */
import { BoardData } from '../../types/kanban/boards';
import { User } from '../../types/kanban/users';

export const isUser = (o: any): o is User => {
  return (
    o._id
        && o.username
        && o.passwordhash
        && o.boards
  );
};

export const isBoard = (o: any): o is BoardData => {
  return (
    o._id
        && o.name
        && o.description
        && o.users
        && o.lists
  );
};

export const isBoardArray = (o: any): o is BoardData[] => {
  return (Array.isArray(o) && o.every(i => isBoard(i)));
};
