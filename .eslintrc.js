module.exports = {
  env: {
    browser: true,
    es2020: true,
  },
  extends: [
    'plugin:react/recommended',
    'airbnb',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 11,
    sourceType: 'module',
  },
  plugins: [
    'react',
    '@typescript-eslint',
    'jest'
  ],
  rules: {
    "padded-blocks": "off",
    "linebreak-style": "off",
    "no-unused-vars": "off",
    "no-console": "off",
    "no-alert": "off",
    "no-multiple-empty-lines": "off",
    "arrow-body-style": "off",
    "react/jsx-filename-extension": "off",
    "import/no-unresolved": "off",
    "import/extensions": "off",
    "react/prop-types": "off",
    "react/destructuring-assignment": "off",
    "arrow-parens": "off",
    "react/jsx-props-no-spreading": "off",
    "max-len": "off"
  },
};
